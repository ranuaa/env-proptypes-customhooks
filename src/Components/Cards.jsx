import React from "react";
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import { PropTypes } from 'prop-types'



const Cards = ({ title, date, desc, img }) => {


    return (
        <Card sx={{ maxWidth: 325, margin: "10px" }}>
            <CardHeader
                avatar={
                    <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                        R
                    </Avatar>
                }
                title={title}
                subheader={date}
            />
            <CardMedia
                component="img"
                height="194"
                image={img}
                alt="Paella dish"
            />
            <CardContent>
                <Typography sx={{ fontFamily: 'Ubuntu' }} variant="body2" color="text.secondary">
                    {desc}
                </Typography>
            </CardContent>
        </Card>
    );
};

Cards.propTypes = {
    title: PropTypes.string,
    datePost: PropTypes.string,
    img: PropTypes.string,
    description: PropTypes.string,
}


export default Cards;
