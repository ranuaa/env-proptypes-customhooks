import { Box, Container } from "@mui/material";
import axios from "axios";
import React, { useState } from "react";
import { useEffect } from "react";
import { useAsync } from "react-use";
import Cards from "../Components/Cards";
import Lists from "../Components/Lists";

const LandingPage = () => {
    const [datas, setDatas] = useState()

    // const handleFetch = async () => {
    //     const datas = useAsync(async () => {
    //         const response = await  axios.get(process.env.REACT_APP_API);
    //         
    //       }
    // }


    // const handleFetch = useAsync(async () => {
    //     const data = await fetch(process.env.REACT_APP_API);
    //     setDatas(data.data)
    // }, []);

    // useEffect(() => {
    //     handleFetch()
    // }, []);

    const handleFetch = useAsync(async () => {
        const data = await axios.get(process.env.REACT_APP_API);
        const waitFor = delay => new Promise(resolve => setTimeout(resolve, delay));
        await waitFor(5000);
        setDatas(data.data)
    }, [])

    console.log(datas)

    return (
        <Container sx={{ display: { md: "flex" } }}>
            {
                handleFetch.loading ? <Box
                    className='right'
                    style={{
                        width: "75%",
                        display: 'flex',
                        flexDirection: "column",
                        padding: '25px',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                    <h1 style={{ marginTop: "15%" }}>Loading</h1>
                    <lottie-player src={"https://assets2.lottiefiles.com/packages/lf20_poqmycwy.json"} background="transparent" speed="1" style={{ width: "300px", height: "300px" }} loop autoplay></lottie-player>
                </Box>
                    : handleFetch.error ? <Box
                        className='right'
                        style={{
                            width: "75%",
                            display: 'flex',
                            flexWrap: "wrap",
                            gap: '10px',
                            padding: '25px',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                        <h1 style={{ marginTop: "15%" }}>{handleFetch.error.message}</h1>
                        <lottie-player src={"https://assets9.lottiefiles.com/packages/lf20_pNx6yH.json"} background="transparent" speed="1" style={{ width: "300px", height: "300px" }} loop autoplay></lottie-player>
                    </Box>
                        :
                        <Box sx={{ display: 'flex', flexWrap: 'wrap', maxWidth: { md: "75%", sm: "100% " }, margin: '5px solid pink' }} >
                            {datas?.map((data) => {
                                return (
                                    <Cards key={data.post_id} title={data.title} date={data.datePost} desc={data.description} img={data.img} />
                                )
                            })}
                        </Box>
            }
            < Box sx={{ display: { md: 'block', sm: 'none' } }}>
                <Lists />
            </Box>

        </Container >
    );
};

export default LandingPage;
